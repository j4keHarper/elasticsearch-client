<?php
/**
 *
 * @author Markus Kleint <markus.kleint@gmail.com>
 * @since  11.12.16
 * PHP 5.5
 *
 */

namespace ElasticsearchClient\Test\Service;

use Elasticsearch\ClientBuilder;
use ElasticsearchClient\Model\Index;
use ElasticsearchClient\Service\BulkIndexer;
use ElasticsearchClient\Service\IndexManager;

class BulkIndexerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var array
     */
    private $testDocumentData;

    /**
     * @var IndexManager
     */
    private $indexManager;

    protected function setUp()
    {
        parent::setUp();

        $this->getIndexManager()
             ->recreateIndex($this->getIndex());
    }

    public function testIndex()
    {
        $client = $this->buildClient();

        $testDocumentCount = 100000;
        $bulkIndexer = new BulkIndexer(10000);

        $testDocument = $this->getTestDocument();


        for ($i = 0; $i < $testDocumentCount; $i++) {

            $bulkIndexer->index($this->getIndex(), 'mytype', $testDocument);
        }
        $bulkIndexer->flush();
        $this->getIndexManager()
             ->refresh($this->getIndex());

        $count = $client->count(
            [
                'index' => $this->getIndex()
                                ->getName(),
                'type'  => 'mytype',
            ]
        );

        static::assertSame(
            $testDocumentCount,
            $count['count'],
            'expecting one document in testindex after BulkIndexer::index-call'
        );
    }

    /**
     * @return IndexManager
     */
    private function getIndexManager()
    {
        if (null === $this->indexManager) {

            $this->indexManager = new IndexManager();
        }

        return $this->indexManager;
    }

    /**
     * @return \Elasticsearch\Client
     */
    private function buildClient()
    {
        return ClientBuilder::create()
                            ->setHosts(['localhost:9200'])
                            ->build();
    }

    /**
     * @return Index
     */
    private function getIndex()
    {
        return new Index('testindex');
    }

    /**
     * @return array
     */
    private function getTestDocument()
    {
        if (null === $this->testDocumentData) {
            $this->testDocumentData = json_decode($this->getExampleJson(), true);
        }

        return $this->testDocumentData;
    }

    private function getExampleJson()
    {
        return /* @lang JSON */<<<JSON
{
    "aggregationRequired": false,
    "articleNormNumber": "asf341C",
    "articleNumber": "aR-1234-d",
    "category": {
        "id": 3495,
        "leafNumber": 452,
        "title": "g werSFASDFGA"
    },
    "clickCount": 0,
    "countryPriceSorting": {"DE": {
        "activeOfferCount": 1,
        "cheapestStrategicPrice": 18.27,
        "merchantTextId": 152481924,
        "offerCount": 1
    }},
    "deliveryCountries": ["DE"],
    "eanNumbers": ["8715616210911"],
    "generalArticleData": {
        "sparePartStatus": {
            "date": "2016-07-25T00:00:00-0400",
            "statusId": 1,
            "title": "Normal"
        },
        "vpe": 1
    },
    "generalTecdocAttributes": [
        {
            "abbreviation": "AEFQGGASFDADsgsd",
            "attributeNumber": 232,
            "displayImmediately": true,
            "sortNumber": 1,
            "title": "AEFQGGASFDADsgsd",
            "value": "1",
            "valueLabel": "Voll"
        },
        {
            "abbreviation": "Lochanzahl",
            "attributeNumber": 500,
            "displayImmediately": true,
            "sortNumber": 2,
            "title": "Lochanzahl",
            "value": "4"
        },
        {
            "abbreviation": "Bremsscheibendicke",
            "attributeNumber": 274,
            "displayImmediately": true,
            "sortNumber": 3,
            "title": "Bremsscheibendicke [mm]",
            "value": "10"
        },
        {
            "abbreviation": "Höhe",
            "attributeNumber": 209,
            "displayImmediately": true,
            "sortNumber": 4,
            "title": "Höhe [mm]",
            "value": "40,4"
        },
        {
            "abbreviation": "Ø",
            "attributeNumber": 200,
            "displayImmediately": true,
            "sortNumber": 5,
            "title": "Durchmesser [mm]",
            "value": "258"
        },
        {
            "abbreviation": "Naben-Ø 1",
            "attributeNumber": 1008,
            "displayImmediately": true,
            "sortNumber": 6,
            "title": "Nabendurchmesser 1 [mm]",
            "value": "76"
        },
        {
            "abbreviation": "Naben-Ø 2",
            "attributeNumber": 1009,
            "displayImmediately": true,
            "sortNumber": 7,
            "title": "Nabendurchmesser 2 [mm]",
            "value": "165"
        },
        {
            "abbreviation": "Lochkreis-Ø",
            "attributeNumber": 560,
            "displayImmediately": true,
            "sortNumber": 8,
            "title": "Lochkreis-Ø [mm]",
            "value": "114"
        },
        {
            "abbreviation": "Ausstattungsvar.",
            "attributeNumber": 3,
            "displayImmediately": true,
            "sortNumber": 9,
            "title": "Ausstattungsvariante",
            "value": "Coated"
        }
    ],
    "id": "7958_BR3208C_15972",
    "isBlockedForTecdoc": false,
    "isRootSparePart": false,
    "isTecdocAttempted": true,
    "isTecdocReceived": true,
    "lastAggregationDate": "2016-12-10T09:56:54-0500",
    "lastAugmentationDate": "2016-12-10T09:56:54-0500",
    "lastAugmentationTimestamp": 1481381814,
    "manufacturer": {
        "dlnr": 201,
        "id": 7958,
        "isPrimary": 1,
        "name": "KAVO PARTS",
        "quality": "unknown",
        "seoName": "KAVO-PARTS"
    },
    "masterId": "201_BR3208C",
    "merchantDescriptionTexts": [{
        "id": 152481924,
        "imageFile": {"url": "https://www.webisco.de/tecdoc/document.php?a=BR-3208-C&e=201&n=0&w=500&c=73aec4705745e43c49c7a11d53bf7d14"},
        "text": "Beschichtete Bremsscheibe"
    }],
    "offers": [{
        "clickCount": 0,
        "externalUrl": "www.japan-parts.net?webiscoparams=YToyOntzOjE6ImEiO3M6NzoiMTI3ODAxNCI7czoyOiJ3YSI7czoyOiJzYSI7fQ%3D%3D",
        "id": 152481924,
        "price": 18.27,
        "shop": {
            "id": 95,
            "isTransaction": true,
            "name": "Japan Parts",
            "status": 1
        }
    }],
    "searchData": [{
        "numberFacets": [{
            "facetName": "manufacturer_id",
            "facetValue": 7958
        }],
        "stringFacets": [
            {
                "facetName": "Einbauort",
                "facetValue": "Hinterachse"
            },
            {
                "facetName": "Versand nach",
                "facetValue": "DE"
            },
            {
                "facetName": "Marken",
                "facetValue": "KAVO PARTS"
            },
            {
                "facetName": "Markenart",
                "facetValue": "Günstige Marken"
            }
        ]
    }],
    "sparePartId": "7958_BR3208C",
    "tecdocArticleImages": [{
        "imageFiles": [
            {
                "url": "http://cdn.daparto.de/0201/880/880_BR-3208-C.jpg",
                "width": 880
            },
            {
                "url": "http://cdn.daparto.de/0201/450/450_BR-3208-C.jpg",
                "width": 450
            }
        ],
        "originalUrl": "http://cdn.daparto.de/0201/original/BR-3208-C.JPG",
        "sortNumber": 1
    }],
    "tecdocArticleVehicle": {
        "genericArticle": {
            "genArtNr": 82,
            "title": "Bremsscheibe"
        },
        "genericArticleNumber": 82,
        "laufendeNr": 2,
        "tecdocVehicle": {
            "constructionYearFrom": "2001-04-01T00:00:00-0500",
            "constructionYearTo": "2006-07-01T00:00:00-0400",
            "hsnTsns": ["8252421"],
            "kTypNr": 15972,
            "vehicleTitle": "HYUNDAI ELANTRA Stufenheck (XD) 2.0 CRDi"
        },
        "vehicleArticleAttributes": [
            {
                "abbreviation": "Einbauort",
                "attributeNumber": 100,
                "displayImmediately": true,
                "sortNumber": 1,
                "title": "Einbauseite",
                "value": "HA",
                "valueLabel": "Hinterachse"
            },
            {
                "attributeNumber": 514,
                "displayImmediately": true,
                "sortNumber": 2,
                "title": "Brems-/Fahrdynamik",
                "value": "1",
                "valueLabel": "für Fahrzeuge mit ABS"
            }
        ]
    },
    "title": "Beschichtete Bremsscheibe"
}
JSON;

    }

}