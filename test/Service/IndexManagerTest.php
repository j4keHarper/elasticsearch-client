<?php
/**
 *
 * @author Markus Kleint <markus.kleint@gmail.com>
 * @since  11.12.16
 * PHP 5.5
 *
 */

namespace ElasticsearchClient\Test\Service;

use ElasticsearchClient\Service\IndexManager;
use ElasticsearchClient\Model\Index;

class IndexManagerTest extends \PHPUnit_Framework_TestCase
{

    const TEST_INDEX_NAME = 'testindex';

    /**
     * @return IndexManager
     */
    public function testCreateIndex()
    {
        $indexManager = new IndexManager();
        $indexManager->dropIndexIfExists(new Index(self::TEST_INDEX_NAME));

        static::assertFalse(
            $indexManager->indexExists(new Index(self::TEST_INDEX_NAME)),
            'test index should not be there in the beginning'
        );

        $indexManager->createIndex(new Index(self::TEST_INDEX_NAME));

        static::assertTrue(
            $indexManager->indexExists(new Index(self::TEST_INDEX_NAME)),
            'test index was not created'
        );

        return $indexManager;

    }

    /**
     * @param IndexManager $indexManager
     *
     * @depends testCreateIndex
     *
     * @return IndexManager
     */
    public function testRecreateIndex(IndexManager $indexManager)
    {
        $indexManager->recreateIndex(new Index(self::TEST_INDEX_NAME));

        static::assertTrue(
            $indexManager->indexExists(new Index(self::TEST_INDEX_NAME)),
            'index should exist (no duplicate index exception should be thrown'
        );

        return $indexManager;
    }

    /**
     * @param IndexManager $indexManager
     *
     * @depends testRecreateIndex
     *
     * @return IndexManager
     */
    public function testDropIndex(IndexManager $indexManager)
    {
        static::assertTrue(
            $indexManager->indexExists(new Index(self::TEST_INDEX_NAME)),
            'index should exist'
        );


        $indexManager->dropIndex(new Index(self::TEST_INDEX_NAME));

        static::assertFalse(
            $indexManager->indexExists(new Index(self::TEST_INDEX_NAME)),
            'index should have been deleted'
        );

        return $indexManager;
    }

    /**
     * @depends testDropIndex
     *
     * @param IndexManager $indexManager
     */
    public function testDropIfExists(IndexManager $indexManager)
    {

        $indexManager->createIndex(new Index(self::TEST_INDEX_NAME));

        static::assertTrue(
            $indexManager->indexExists(new Index(self::TEST_INDEX_NAME)),
            'index was not created'
        );

        $indexManager->dropIndexIfExists(new Index(self::TEST_INDEX_NAME));

        static::assertFalse(
            $indexManager->indexExists(new Index(self::TEST_INDEX_NAME)),
            'index was not deleted'
        );

        //once again to make sure, no exception is raised on droppIfExists on not existing index

        $indexManager->dropIndexIfExists(new Index(self::TEST_INDEX_NAME));

        static::assertFalse(
            $indexManager->indexExists(new Index(self::TEST_INDEX_NAME)),
            'index was not dropped (2nd time)'
        );
    }
}