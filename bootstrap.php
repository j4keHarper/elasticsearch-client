<?php
/**
 *
 * @author Markus Kleint <markus.kleint@gmail.com>
 * @since  28.11.16
 * PHP 5.5
 *
 */
$loader = require __DIR__ . '/vendor/autoload.php';
$loader->add('ElasticsearchClient', __DIR__ . '/src/');
$loader->add('ElasticsearchClient\\Test', __DIR__ . '/src/');