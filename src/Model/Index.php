<?php
/**
 *
 * @author Markus Kleint <markus.kleint@gmail.com>
 * @since  11.12.16
 * PHP 5.5
 *
 */

namespace ElasticsearchClient\Model;

class Index
{

    /**
     * @var string
     */
    private $name;

    /**
     * Index constructor.
     *
     * @param string $name
     */
    public function __construct($name)
    {
        $this->setName($name);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

}