<?php
/**
 *
 * @author Markus Kleint <markus.kleint@gmail.com>
 * @since  11.12.16
 * PHP 5.5
 *
 */

namespace ElasticsearchClient\Service;

use Elasticsearch\ClientBuilder;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use ElasticsearchClient\Model\Index;

class IndexManager
{

    /**
     * @param Index $index
     */
    public function createIndex(Index $index)
    {
        $this->buildClient()
             ->indices()
             ->create(
                 ['index' => $index->getName()]
             );
    }

    /**
     * @param Index $index
     */
    public function dropIndex(Index $index)
    {

        $this->buildClient()
             ->indices()
             ->delete(
                 ['index' => $index->getName()]
             );
    }

    /**
     * @param Index $index
     *
     * @return bool
     */
    public function indexExists(Index $index)
    {
        return $exists = $this->buildClient()
            ->indices()
            ->exists(
                ['index' => $index->getName()]
            );
    }

    /**
     * @param Index $index
     */
    public function refresh(Index $index)
    {
        $this->buildClient()
            ->indices()
            ->refresh(
                ['index' => $index->getName()]
            );
    }

    /**
     * @param Index $index
     */
    public function recreateIndex(Index $index)
    {
        $this->dropIndexIfExists($index);
        $this->createIndex($index);
    }

    /**
     * @param Index $index
     */
    public function dropIndexIfExists(Index $index)
    {
        try {

            $this->dropIndex($index);

        } catch(Missing404Exception $e) {

            //index seems to not was found.
        }
    }

    /**
     * @return \Elasticsearch\Client
     */
    private function buildClient()
    {
        return ClientBuilder::create()
                            ->setHosts(['localhost:9200'])
                            ->build();
    }

}