<?php
/**
 *
 * @author Markus Kleint <markus.kleint@gmail.com>
 * @since  11.12.16
 * PHP 5.5
 *
 */

namespace ElasticsearchClient\Service;

use Elasticsearch\ClientBuilder;
use ElasticsearchClient\Model\Index;

class BulkIndexer
{
    const DEFAULT_MAX_BULK_SIZE = 10;

    /**
     * @var array
     */
    private $bulk = ['body' => []];

    /**
     * @var int
     */
    private $currentBulkMemoryConsumption = 0;

    /**
     * @var int
     */
    private $maxBulkMemoryConsumption; // KB * MB * GB

    /**
     * @var int
     */
    private $maxBulkSize;

    /**
     * BulkIndexer constructor.
     *
     * @param int $maxBulkSize
     */
    public function __construct($maxBulkSize = self::DEFAULT_MAX_BULK_SIZE)
    {
        $this->setMaxBulkSize($maxBulkSize);
        $this->maxBulkMemoryConsumption = 80 * (1024  * 1024);
    }

    /**
     * @param Index  $index
     * @param string $type
     * @param array  $document
     */
    public function index(Index $index, $type, array $document)
    {
        $metaData = [
            'index' => [
                '_index' => $index->getName(),
                '_type' => $type,
            ]
        ];

        $this->bulk['body'][] = $metaData;
        $this->currentBulkMemoryConsumption += strlen(json_encode($metaData));

        $this->bulk['body'][] = $document;
        $this->currentBulkMemoryConsumption += strlen(json_encode($document));

        if (count($this->bulk['body']) >= $this->getMaxBulkSize() * 2) {

            $this->flush();
        }

        if ($this->currentBulkMemoryConsumption >= $this->maxBulkMemoryConsumption) {

            $this->logImplicitFlush();
            $this->flush();
        }
    }

    public function flush()
    {
        if (count($this->bulk['body'])) {

            $this->buildClient()
                ->bulk($this->bulk);

        }
        $this->bulk = ['body' => []];
        $this->currentBulkMemoryConsumption = 0;
    }

    /**
     * @return int
     */
    public function getMaxBulkSize()
    {
        return $this->maxBulkSize;
    }

    /**
     * @param int $maxBulkSize
     *
     * @return $this
     */
    public function setMaxBulkSize($maxBulkSize)
    {
        $this->maxBulkSize = $maxBulkSize;

        return $this;
    }


    /**
     * @return \Elasticsearch\Client
     */
    private function buildClient()
    {
        return ClientBuilder::create()
                            ->setHosts(['localhost:9200'])
                            ->build();
    }

    private function logImplicitFlush()
    {
        echo sprintf(
            'ignoring bulksize of [%s] - doing implicit flush at bulk-size [%s] to avoid memory error' . PHP_EOL,
            number_format(($this->getMaxBulkSize()), 0, ',', '.'),
            number_format(count($this->bulk['body']) / 2, 0, ',', '.')
        );
    }

}